package impls

import interfaces.Functions

/**
 * Класс, реализующий интерфейс Functions
 */
class FunctionsImpl : Functions {
    override fun calculate(a: Int, b: Int) = a + b
    override fun substringCounter(s: String, sub: String): Int {
        var index = 0
        var count = 0

        while (true)
        {
            index = s.indexOf(sub, index)
            index += if (index != -1)
            {
                count++
                sub.length
            }
            else {
                return count
            }
        }
    }

    override fun splitterSorter(s: String, sub: String): List<String> {
        return s.split(sub)
    }

    override fun uniqueCounter(s: String, sub: String): Map<String, Int> {
        val words = s.split(sub)

        return words.groupBy { it }
            .map { (value, eqVals) -> (value to eqVals.size) }
            .toMap()
    }

    override fun isPalindrome(s: String): Boolean {
        if (s.isEmpty())
            return false
        for (i in 0..s.length / 2) {
            if (s[i] != s[s.length - i - 1]) return false
        }
        return true
    }

    override fun invert(s: String): String {
        var result: String = ""
        var lastIndex = s.lastIndex

        while (lastIndex >= 0) {
            result += s[lastIndex]
            lastIndex--
        }

        return result;
    }
}
